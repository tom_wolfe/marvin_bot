#!/usr/bin/env python3

import os, sys

from bs4 import BeautifulSoup as BS
from mastodon import Mastodon
from argparse import ArgumentParser
from PIL import Image
import requests
import numpy as np
from io import BytesIO
from io import StringIO
import feedparser

sys.stdout.write("Hello, I am Marvin, the robot\n\n")

parser = ArgumentParser()

parser.add_argument("-cc", "--client-credentials", dest="clientCredentials",
                    help="ABSOLUTE path to the clientcred.txt", metavar="FILE", required=True)
parser.add_argument("-uc", "--user-credentials", dest="userCredentials",
                    help="ABSOLUTE path to the usercred.txt", metavar="FILE", required=True)
parser.add_argument("-url", "--rss-url", dest="rssUrl",
                    help="URL to the RSS feed you wish to pull from (start with http:// or https://)", required=True)
parser.add_argument("-m", "--mastodon-instance", dest="instanceUrl",
                    help="URL to your instance (start with http:// or https://)", required=True)
parser.add_argument("-user", "--user-email", dest="userEmail",
                    help="Login email (not needed if -uc and -uc args are valid)")
parser.add_argument("-pass", "--user-password", dest="userPassword",
                    help="Login password (not needed if -uc and -uc args are valid)")
parser.add_argument('-max', "--maximum-bulk-posts", dest="bulkPostMaxCount",
                    help="Maximum numbder of posts Marvin will post per one run (to avoid timeline congestion)", type=int)

args = parser.parse_args()

bulkPostMaxCount = args.bulkPostMaxCount
if bulkPostMaxCount == None:
    bulkPostMaxCount = 5

url = args.rssUrl;
feed = feedparser.parse(url)
stories = feed["entries"]

def post_image(url):
    uploadFile = os.getcwd() + "/upload.png"
    if os.path.isfile(uploadFile):
        os.remove(uploadFile)
    print("Opening image at ", url)
    print("Saving it to ", uploadFile)
    response = requests.get(url, stream=True).raw
    img=Image.open(response)
    img.save(uploadFile)
    image_bytes = open(uploadFile, "rb").read()
    return mastodon.media_post(image_bytes, "image/png")

def seen(myid):
    try:
        with open('seen.db', 'r') as f:
            if (str(myid) in [x.strip() for x in f.readlines()]):
                # If story is in the list of the seen stories - return 1
                return 1
            else:
                return 0
    except IOError as e:
        # catch non-existing SEENDB
        if e.errno == 2:
            return 0

def write_to_seen(myid):
    with open('seen.db', 'a') as f:
        f.write(str(myid))
        f.write('\n')

# Make the list of stories who's id isn't in the seen list
unseen_stories = [story for story in stories if not seen(story["id"])]


try:
    instance_url = args.instanceUrl;
except:
    instance_url = sys.argv[3];

# Create app if doesn't exist
if not os.path.isfile(args.clientCredentials):
    print("Creating app")
    mastodon = Mastodon.create_app(
        'MarvinBot',
        to_file = args.clientCredentials,
        api_base_url=args.instanceUrl
    )

# Fetch access token if I didn't already
if not os.path.isfile(args.userCredentials):
    print("Logging in")
    mastodon = Mastodon(
        client_id = args.clientCredentials,
        api_base_url=args.instanceUrl
    )
    email = args.userEmail
    password = args.userPassword
    mastodon.log_in(email, password, to_file = args.userCredentials)

# Login using generated auth
mastodon = Mastodon(
    client_id = args.clientCredentials,
    access_token = args.userCredentials,
    api_base_url=args.instanceUrl
)

counter = 0
unseenCount = len(unseen_stories)
print("I'm processing", unseenCount, "unseen stories. Hang on!")
for story in unseen_stories:
    counter = counter + 1
    if counter <= bulkPostMaxCount:
        soup = BS(story["description"], "html.parser")
        images=soup.find_all('img')
        image=""
        preview=""
        result="I couldn't find any images :("
        if len(images)>0:
            result=post_image(str(images[0]["src"]))
        print("\n", result, "\n")
        title = story["title"]
        storyid = story["id"]
        link = story["link"]
        post = title + "\n-\n" + link + "\n-\n#marvinbot"
        write_to_seen(storyid)
        print("Posting " + post)
        if result == None:
            mastodon.toot(post)
        else:
            mastodon.status_post( post, in_reply_to_id = None, media_ids = [result["id"]] )
