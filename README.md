# README #

### MarvinBot for Mastodon ###

Marvin is a little RSS bot written in Python, for use on Mastodon Instances. Wrote it for my instance https://mastodon.wolfe.cloud as a little exercise.

You are free to use it, modify it and do whatever you want with it.

See it in action there as the @tooterbot ( https://mastodon.wolfe.cloud/@tooterbot )

**Version**

0.1a

**Credit**

Used the code here https://github.com/raymestalez/mastodon-hnbot as a starting point

**How-To**

Make marvinbot.py executable and run it without params to see what it needs. Once you've run it once with your credentials supplied, it'll create files with tokens for logging in to Mastodon (and you'll see it appear as an approved app in your Settings).

Once that's out of the way, you can modify the run.sh to fit your needs (and add several calls to different RSS feeds, if you'd like) and use cron to execute it periodically to keep your feed up to date. 

That's about it folks :)

Have fun